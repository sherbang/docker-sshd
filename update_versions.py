#!/usr/bin/env python3

import re

import packaging
import yaml
from sh import apk


def parse_alpine_version(apk_str):

    match = re.match(r"openssh-(\d+).(\d+)_p(\d+)-r(\d+)\s", apk_str)
    assert match is not None
    raw_version_tuple = match.groups()

    return raw_version_tuple


def main():
    apk_process = apk.list("-i", "openssh")
    apk_str = apk_process.next()

    version_tuple = parse_alpine_version(apk_str)

    with open("version.txt", "w") as versiontxt:
        versiontxt.write(".".join(version_tuple))


if __name__ == "__main__":
    main()
