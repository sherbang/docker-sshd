#!/bin/bash

export CONTAINER_ID=$(docker run -d -e "USERS_B64=$(base64 -w 0 test/users.yaml)" -e "SSH_HOST_RSA_KEY=$(cat test/hostkeys/ssh_host_rsa_key)" -p 2222:22 sherbang/sshd)

# Wait up to 10 seconds for the ssh server to be available.
timeout 10 bash -c 'until head --lines=1 2>>/dev/null </dev/tcp/$0/$1 | grep "^SSH" ; do sleep 0.1; done' localhost 2222

CONTAINER_IP=$(docker network inspect bridge | yq eval '.[] | select(.Name == "bridge").Containers[strenv(CONTAINER_ID)].IPv4Address' - | sed 's/\/.*//')

# docker exec $CONTAINER_ID find /home/bob/

docker run -it --rm positronsecurity/ssh-audit $CONTAINER_IP

# Check that login as bob worked (should print "LOGIN SUCCESS")
ssh -p 2222 -i test/bob.key -o "UserKnownHostsFile test/known_hosts" bob@localhost echo "'LOGIN SUCCESS (PASS)'" || (FAILED_TEST=1 ; echo 'Bob login failed (FAIL)')

# Check that login as fred fails with wrong command
ssh -p 2222 -i test/bob.key -o "UserKnownHostsFile test/known_hosts" fred@localhost echo "Allowed unpermitted command (FAIL)" | grep '^foo$' >>/dev/null && echo 'authorized_keys command (PASS)' || FAILED_TEST=1 

if [ ! -z "$FAILED_TEST" ] ; then
    echo ------CONTAINER LOGS-----

    docker logs $CONTAINER_ID
fi

docker stop $CONTAINER_ID
docker rm $CONTAINER_ID

docker build -t sshd-pytest -f Dockerfile.pytest .

docker run --rm -it -v $(pwd):/src sshd-pytest /src/pytest.sh
