#!/bin/bash

# Exit on first failing command.
set -e

while getopts "pl" o
do
    case $o in
    p)
        PUSH=1
        ;;
    l)
        PUSH_LATEST=1
        ;;
    \?)
        echo "Usage: $0 [-p]" >&2
        echo "" >&2
        echo " -p - Push created image to default repository"  >&2
        echo " -l - Push created image to default repository as 'latest'"  >&2
		exit 1
        ;;
	esac
done

# Print commands
set -x

docker build -t sherbang/sshd docker-context
VERSION=$(docker run --rm --entrypoint=/print-versions.sh sherbang/sshd)
docker tag sherbang/sshd sherbang/sshd:$VERSION

./test.sh

if [ ! -z "$PUSH" ] ; then
    docker push sherbang/sshd:$VERSION
fi

if [ ! -z "$PUSH_LATEST" ] ; then
    docker push sherbang/sshd:latest
fi
