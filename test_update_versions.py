from update_versions import parse_alpine_version


def test_first():
    apk_str = "openssh-8.6_p1-r2 x86_64 {openssh} (BSD)\n"
    version_tuple = parse_alpine_version(apk_str)

    assert version_tuple == ('8', '6', '1', '2')
