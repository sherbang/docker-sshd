# Alpine Linux SSHD

OpenSSH Docker image with [aah-audit](https://www.ssh-audit.com/) security hardening.

# ENV Variables
- USERS - yaml user data (see User Definitions below)
- USERS_B64 - if you need to provide the $USERS data base64 encoded

Host keys: (if none of these are defined then new keys will be randomly generated)
- SSH_HOST_RSA_KEY - contents of /etc/ssh/ssh_host_rsa_key
- SSH_HOST_ED25519_KEY - contents of /etc/ssh/ssh_host_ed25519_key
- SSH_HOST_RSA_KEY_B64 - base64 encoded contents of /etc/ssh/ssh_host_rsa_key
- SSH_HOST_ED25519_KEY_B64 - base64 encoded contents of /etc/ssh/ssh_host_ed25519_key

(if both the plain and base64 encoded values are defined for the same variable then the _B64 values take precidence)

## User Definitions Example
To create users pass a USERS env that contains a yaml list of usernames and authorized keys:
```yaml
- username: bob
  authorized_keys: ssh-rsa <pubkey data> bob
- username: chuck
  authorized_keys: |
      ssh-rsa <some pubkey> chuck@foo
      ssh-rsa <another pubkey> chuck@bar
```

# Basic Usage

Create users.yaml (look in test/ for example).

```bash
$ docker run --rm --env "USERS=$(cat users.yaml)" --publish=2222:22 sherbang/sshd
Host keys:
<..>
Server listening on 0.0.0.0 port 22.
Server listening on :: port 22.

$ ssh bob@localhost -p 2222  # or $(docker-machine ip default)
```

Any arguments are passed to `sshd`.
```

# Version Info

```bash
$ docker run --rm --entrypoint=/print-versions.sh sherbang/sshd
alpine-3.14.0_openssh-8.6_p1-r2
```
