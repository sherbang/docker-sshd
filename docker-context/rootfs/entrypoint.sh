#!/bin/ash

# Exit on first failing command.
set -e

# Print commands
#set -x

if [ ! -z "$SSH_HOST_RSA_KEY" ] ; then
  echo "$SSH_HOST_RSA_KEY" > /etc/ssh/ssh_host_rsa_key
  chmod 600 /etc/ssh/ssh_host_rsa_key
  host_keys=1
fi
if [ ! -z "$SSH_HOST_ED25519_KEY" ] ; then
  echo "$SSH_HOST_ED25519_KEY" > /etc/ssh/ssh_host_ed25519_key
  chmod 600 /etc/ssh/ssh_host_ed25519_key
  host_keys=1
fi

# Decoding the _B64 variables into the plain ones would be more DRY, but this will let us use base64 encoding to work around potential shell escaping bugs.
if [ ! -z "$SSH_HOST_RSA_KEY_B64" ] ; then
  echo "$SSH_HOST_RSA_KEY_B64" | base64 -d - > /etc/ssh/ssh_host_rsa_key
  chmod 600 /etc/ssh/ssh_host_rsa_key
  host_keys=1
fi
if [ ! -z "$SSH_HOST_ED25519_KEY_B64" ] ; then
  echo "$SSH_HOST_ED25519_KEY_B64" | base64 -d - > /etc/ssh/ssh_host_ed25519_key
  chmod 600 /etc/ssh/ssh_host_ed25519_key
  host_keys=1
fi

# generate host keys if not present and none were provided
if [ -z "$host_keys" ] ; then
  ssh-keygen -q -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N "" < /dev/null
  ssh-keygen -q -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N "" < /dev/null
fi

echo Host keys: >&2
if [ -f /etc/ssh/ssh_host_rsa_key ] ; then
  ssh-keygen -y -f /etc/ssh/ssh_host_rsa_key
fi
if [ -f /etc/ssh/ssh_host_ed25519_key ] ; then
  ssh-keygen -y -f /etc/ssh/ssh_host_ed25519_key
fi

if [ ! -z "$USERS_B64" ] ; then
  export USERS=$(echo $USERS_B64 | base64 -d -)
fi

# Add users from $USERS
yq e --null-input 'env(USERS) | .[].username' | while read username; do
    export username
    yq e -n '.username = strenv(username)'
    adduser -D -s /bin/ash $username
    passwd -u $username
    mkdir -p /home/$username/.ssh
    yq e --null-input 'env(USERS) | .[] | select(.username == strenv(username)).authorized_keys' > /home/$username/.ssh/authorized_keys
    chmod 600 /home/$username/.ssh/authorized_keys
    chown -R $username /home/$username
done
unset username

# do not detach (-D), log to stderr (-e), passthrough other arguments
exec /usr/sbin/sshd -D -e "$@"
