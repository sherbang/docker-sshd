#!/bin/sh

echo -n 'alpine-'
cat /etc/alpine-release | tr -d "\n"
echo -n '_'
apk list -i openssh 2>/dev/null | awk '{print $1}'
